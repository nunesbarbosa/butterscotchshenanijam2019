﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace ShenniJam
{

    public class MainMenuInterfaceManager : MonoBehaviour
    {
        private void Start()
        {
            AudioManager.instance.PlayMainMenuAudio();
        }

        public void OnCredits()
        {
            SceneManager.LoadScene("Credits");
        }

        public void OnStartGame()
        {
            GameManager.instance.StartGame();
            AudioManager.instance.StopMainMenuAudio();

            SceneManager.LoadScene("Level 1");
        }

        public void OnLoadLevel(int level)
        {
            GameManager.instance.currentScene = level;
            GameManager.instance.StartGame();
            AudioManager.instance.StopMainMenuAudio();

            SceneManager.LoadScene("Level " + (level +1));
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Random = UnityEngine.Random;


namespace ShenniJam
{
    public class EnemyPatrol : MonoBehaviour
    {
        public Transform PathContainer;
        public float WaypointOffset = .1f;
        public bool Loop = true;
        public bool IsPaused;

        public delegate void OnMove(Vector2 direction);
        public OnMove onMove;

        List<Transform> _path;
        int _currentWaypoint;
        bool _hasReachedDestination;
        float _stopTime;

        void Awake()
        {
            _path = new List<Transform>();
            if (PathContainer != null)
            {
                foreach (Transform child in PathContainer)
                {
                    _path.Add(child);
                }
            }
            else
            {
                Debug.LogWarning("No path set.");
            }
        }

        void Update()
        {
            if (IsPaused)
                return;

            if (Vector3.Distance(transform.position, _path[_currentWaypoint].position) <= WaypointOffset && !_hasReachedDestination)
            {
                _hasReachedDestination = true;

                PatrolWaypoint patrolWaypoint = _path[_currentWaypoint].GetComponent<PatrolWaypoint>();
                if (patrolWaypoint != null)
                {
                    _stopTime = Random.Range(patrolWaypoint.StopDuration - patrolWaypoint.StopDurationVariation, patrolWaypoint.StopDuration + patrolWaypoint.StopDurationVariation);
                    if (Random.value >= patrolWaypoint.StopProbability)
                    {
                        GoToNextWaypoint();
                    }
                }
                else
                {
                    GoToNextWaypoint();
                }
            }

            if (_hasReachedDestination)
            {
                _stopTime -= Time.deltaTime;

                if (_stopTime <= 0)
                    GoToNextWaypoint();
            }
            else
            {
                Move();
            }

        }

        private void Move()
        {
            var direction = _path[_currentWaypoint].position - transform.position;

            if (onMove != null)
                onMove(direction);
        }

        public void StartPatrol()
        {
            GoToWaypoint(0);
        }

        public void PausePatrol()
        {
            IsPaused = true;
        }

        public void ResumePatrol()
        {
            GoToWaypoint(_currentWaypoint);
        }

        void GoToNextWaypoint()
        {
            if (_currentWaypoint < _path.Count - 1)
            {
                _currentWaypoint++;
            }
            else
            {
                if (Loop)
                {
                    _currentWaypoint = 0;
                }
                else
                {
                    Debug.Log("Path completed");
                }
            }
            GoToWaypoint(_currentWaypoint);
        }

        void GoToWaypoint(int waypoint)
        {
            IsPaused = false;

            _hasReachedDestination = false;
            _currentWaypoint = waypoint;
        }
    }
}
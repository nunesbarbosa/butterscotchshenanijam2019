﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ShenniJam
{
    public class CharacterController : MonoBehaviour
    {
        public GameObject leftLid;
        public GameObject rightLid;

        public float minBlinkTime = 2.5f;
        public float maxBlinkTime = 3.5f;

        public bool canBlink;

        private float leftLidBlinkTime;
        private float rightLidBlinkTime;

        private void Awake()
        {
            UpdateBlink(leftLid);
            UpdateBlink(rightLid);
        }

        void Update()
        {
            if (!canBlink)
                return;

            leftLidBlinkTime -= Time.deltaTime;
            rightLidBlinkTime -= Time.deltaTime;

            if (leftLidBlinkTime <= 0)
                leftLidBlinkTime = UpdateBlink(leftLid);

            if (rightLidBlinkTime <= 0)
                rightLidBlinkTime = UpdateBlink(rightLid);
        }

        public void CloseEyes()
        {
            canBlink = false;

            leftLid.SetActive(true);
            rightLid.SetActive(true);
        }

        public void ResumeBlink()
        {
            canBlink = true;

            UpdateBlink(leftLid);
            UpdateBlink(rightLid);
        }

        private float UpdateBlink(GameObject lid)
        {
            bool isLidOpen = lid.activeInHierarchy;
            lid.SetActive(!isLidOpen);

            return !isLidOpen ? 0.15f : UnityEngine.Random.Range(minBlinkTime, maxBlinkTime);

        }

    }
}
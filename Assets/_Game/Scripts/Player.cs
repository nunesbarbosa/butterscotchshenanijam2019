﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Puppet2D;

namespace ShenniJam
{
    public class Player : MonoBehaviour
    {

        [Header("Player Attributes")]
        public float speed = 2f;
        public bool alive = true;
        public float maxJumpTime = 0.75f;
        public float teleportDuration = 0.1f;
        public float switchDuration = 5f;
        public Collider2D bodyCollider;
        public Collider2D feetCollider;

        private float jumpTime;
        private bool isClone;
        [HideInInspector] public bool isJumping;
        private bool canMove = true;
        private Vector2 direction;
        private Animator animator;
        private CharacterController characterController;
        private Puppet2D_GlobalControl puppet2DGlobalControl;
        private Vector2 initialPosition;

        public delegate void OnTeleport();
        public OnTeleport onTeleport;

        public delegate void OnEndTeleport();
        public OnEndTeleport onEndTeleport;

        public delegate void OnSwitch();
        public OnSwitch onSwitch;

        public delegate void OnEndSwitch();
        public OnSwitch onEndSwitch;

        public delegate void OnDeath();
        public OnDeath onDeath;

        //Portal Related
        [HideInInspector] public GameObject body;
        private Vector3 oldPortal;

        // cached refs
        Rigidbody2D myRB;

        private void Awake()
        {
            myRB = GetComponent<Rigidbody2D>();
            animator = GetComponentInChildren<Animator>();
            characterController = GetComponentInChildren<CharacterController>();
            puppet2DGlobalControl = GetComponentInChildren<Puppet2D_GlobalControl>();
            initialPosition = transform.position;
        }

        private void FixedUpdate()
        {
            if (!canMove)
                return;

            if (alive)
            {
                if (isJumping)
                    Jump();
                else
                    MovePlayer();
            }
        }

        private void Update()
        {
            if (!canMove)
                return;

            if (alive)
            {
                if (/*isClone &&*/ !isJumping && Input.GetButtonDown("Jump") && direction.magnitude > 0)
                {
                    Physics2D.IgnoreLayerCollision(8, 14, true);
                    animator.SetBool("Jump", true);
                    isJumping = true;
                    AudioManager.instance.StopWalkSFX();
                }
            }


        }

        private void MovePlayer()
        {
            direction = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
            myRB.MovePosition(myRB.position + direction.normalized * this.speed * Time.fixedDeltaTime);

            if (direction.x < 0)
                puppet2DGlobalControl.flip = true;
            else if (direction.x > 0)
                puppet2DGlobalControl.flip = false;

            var speed = Mathf.Abs(direction.x) + Mathf.Abs(direction.y);
            animator.SetFloat("Speed", speed);

            if (speed > 0)
            {
                AudioManager.instance.PlayWalkSFX();
            }
            else
            {
                AudioManager.instance.StopWalkSFX();
            }
        }

        private void Jump()
        {
            jumpTime += Time.fixedDeltaTime;
            if (jumpTime >= maxJumpTime)
            {
                Physics2D.IgnoreLayerCollision(8, 14, false);
                animator.SetBool("Jump", false);
                jumpTime = 0;
                isJumping = false;
                return;
            }

            myRB.MovePosition(myRB.position + direction * speed * Time.fixedDeltaTime);
        }

        public void TeleportPlayer(Portal portal, float offset)
        {
            isClone = true;

            canMove = false;
            AudioManager.instance.StopWalkSFX();

            if (onTeleport != null)
                onTeleport();

            StartCoroutine(TeleportPlayerCR(portal, offset));
        }

        public bool PressSwitch()
        {
            if (feetCollider.IsTouchingLayers(LayerMask.GetMask("Switch")))
            {
                canMove = false;
                AudioManager.instance.StopWalkSFX();

                if (onSwitch != null)
                    onSwitch();

                StartCoroutine(PressSwitchCR());

                return true;
            }
            return false;
        }

        private IEnumerator PressSwitchCR()
        {
            yield return new WaitForSeconds(switchDuration);

            canMove = true;

            if (onEndSwitch != null)
                onEndSwitch();
        }

        private IEnumerator TeleportPlayerCR(Portal portal, float offset)
        {
            yield return new WaitForSeconds(teleportDuration);

            oldPortal = transform.position;
            Vector3 newPos = new Vector3(portal.transform.position.x + offset, portal.transform.position.y - 1.2f, portal.transform.position.z);
            transform.position = newPos;

            if (onEndTeleport != null)
                onEndTeleport();

            yield return new WaitForSeconds(0.5f);
            canMove = true;
            AudioManager.instance.ChangeGameAudio();
        }

        public void KillPlayer()
        {
            if (alive)
                AudioManager.instance.PlayDeath();

            canMove = false;
            AudioManager.instance.StopWalkSFX();

            if (onDeath != null)
                onDeath();

            StartCoroutine(KillPlayerCR());
        }

        private IEnumerator KillPlayerCR()
        {
            yield return new WaitForSeconds(1f);
            transform.position = initialPosition;

            yield return new WaitForSeconds(1f);

            //Destroy(body.gameObject);

            isClone = false;
            characterController.ResumeBlink();

            canMove = true;

            AudioManager.instance.ResetGameAudio();
        }

    }
}

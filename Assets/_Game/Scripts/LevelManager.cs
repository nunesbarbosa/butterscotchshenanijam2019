﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Com.LuisPedroFonseca.ProCamera2D;
using System;
using DG.Tweening;

namespace ShenniJam
{
    public class LevelManager : MonoBehaviour
    {
        private void Start()
        {
            GameManager.instance.InitializeLevel();
        }
    }
}

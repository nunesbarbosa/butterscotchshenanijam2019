﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace ShenniJam
{

    public class InGameInterfaceManager : MonoBehaviour
    {
        public GameObject panelSettings;

        private void Update()
        {
            if (Input.GetButtonDown("Cancel"))
            {
                if (Time.timeScale == 0)
                    OnClose();
                else
                    OnSettings();
            }
        }

        public void OnMainMenu()
        {
            Pause(false);

            SceneManager.LoadScene("Main");
        }

        public void OnSettings()
        {
            Pause(true);
            ShowPanelSettings(true);
        }

        public void OnClose()
        {
            Pause(false);
            ShowPanelSettings(false);
        }

        private void ShowPanelSettings(bool isShow)
        {
            panelSettings.SetActive(isShow);
        }

        private void Pause(bool isPause)
        {
            Time.timeScale = isPause ? 0 : 1;
        }

    }
}

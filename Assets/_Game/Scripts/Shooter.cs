﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ShenniJam
{

    public class Shooter : MonoBehaviour
    {
        [Header("Attributes")]
        public float timeToSpawn = 2f;
        public bool shouldShoot = true;
        public Projectile projectile;

        public List<Gun> guns;


        private IEnumerator Start()
        {
            while (shouldShoot)
            {
                yield return new WaitForSeconds(timeToSpawn);
                foreach (var g in guns)
                {
                    if (!g.isActive)
                        continue;

                    var pGo = Instantiate(projectile, g.noozle.position, Quaternion.identity);
                    var p = pGo.GetComponent<Projectile>();
                    p.Initialize(g.direction);
                }
            }
        }

        [Serializable]
        public class Gun
        {
            public Transform noozle;
            public Vector2 direction;
            public bool isActive;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using FMODUnity;
using FMOD.Studio;

namespace ShenniJam
{

    public class AudioManager : UnitySingleton<AudioManager>
    {
        [Header("Main Menu")]
        public string mainMenuEventPath;
        private EventInstance mainMenuInstance;
        public bool isMainMenuPlaying;

        [Header("Game")]
        public string gameEventPath;
        private EventInstance gameInstance;
        public bool isGamePlaying;

        [Header("SFX")]
        public string deathSFX;
        public string teleportSFX;
        public string switchSFX;

        public string walkSFXPath;
        private EventInstance walkInstance;
        public bool isWalkPlaying;

        public void PlayDeath()
        {
            RuntimeManager.PlayOneShot(deathSFX);
        }

        public void PlayTeleport()
        {
            RuntimeManager.PlayOneShot(teleportSFX);
        }

        public void PlaySwitch()
        {
            RuntimeManager.PlayOneShot(switchSFX);
        }

        public void PlayMainMenuAudio()
        {
            if (!isMainMenuPlaying)
            {
                var guid = RuntimeManager.PathToGUID(mainMenuEventPath);
                mainMenuInstance = RuntimeManager.CreateInstance(guid);

                mainMenuInstance.start();

                isMainMenuPlaying = true;
            }
        }

        public void StopMainMenuAudio()
        {
            if (isMainMenuPlaying)
            {
                mainMenuInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
                mainMenuInstance.release();

                isMainMenuPlaying = false;
            }
        }

        public void PlayGameAudio()
        {
            if (!isGamePlaying)
            {
                var guid = RuntimeManager.PathToGUID(gameEventPath);
                gameInstance = RuntimeManager.CreateInstance(guid);

                gameInstance.start();

                isGamePlaying = true;
            }
        }

        public void PlayWalkSFX()
        {
            if (!isWalkPlaying)
            {
                var guid = RuntimeManager.PathToGUID(walkSFXPath);
                walkInstance = RuntimeManager.CreateInstance(guid);

                walkInstance.start();

                isWalkPlaying = true;
            }
        }

        public void StopWalkSFX()
        {
            if (isWalkPlaying)
            {
                walkInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
                walkInstance.release();

                isWalkPlaying = false;
            }
        }

        public void ChangeGameAudio()
        {
            if (isGamePlaying)
            {
                var mutacao = Random.Range(1, 3);
                gameInstance.setParameterByName("mutação", mutacao);
            }
        }

        public void ResetGameAudio()
        {
            if (isGamePlaying)
            {
                gameInstance.setParameterByName("mutação", 0);
            }
        }

        public void StopGameAudio()
        {
            if (isGamePlaying)
            {
                gameInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
                gameInstance.release();

                isGamePlaying = false;
            }
        }


    }
}

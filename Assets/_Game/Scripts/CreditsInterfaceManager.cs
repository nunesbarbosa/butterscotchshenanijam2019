﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace ShenniJam
{

    public class CreditsInterfaceManager : MonoBehaviour
    {
        public void OnBack()
        {
            SceneManager.LoadScene("Main");
        }
    }

}
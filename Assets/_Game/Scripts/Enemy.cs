﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace ShenniJam
{

    public class Enemy : MonoBehaviour
    {
        //Cached Ref
        Collider2D myCollider;

        private void Awake()
        {
            myCollider = GetComponent<Collider2D>();
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            Player player = collision.gameObject.GetComponent<Player>();
            if (!player) return;


            if (myCollider.IsTouching(player.feetCollider) && gameObject.CompareTag("Hole"))
                player.KillPlayer();
            else player.KillPlayer();
        }


    }
}

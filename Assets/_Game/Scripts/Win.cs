﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ShenniJam
{

    public class Win : MonoBehaviour
    {

        private void OnTriggerEnter2D(Collider2D collision)
        {
            Player player = collision.gameObject.GetComponent<Player>();

            if (!player) return;

            player.alive = false;

            GameManager.instance.LoadNextScene();
        }
    }
}

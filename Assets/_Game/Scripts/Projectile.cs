﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace ShenniJam
{

    public class Projectile : MonoBehaviour
    {

        [Header("Attributes")]
        public float speed = 2f;
        public float maxTimeToLive = 30f;

        private Vector2 direction;
        private Rigidbody2D rb2D;
        private float timeToLive;

        private void Awake()
        {
            rb2D = GetComponent<Rigidbody2D>();
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            Destroy(gameObject);
            Player player = collision.gameObject.GetComponent<Player>();
            if (player)
                player.KillPlayer();
        }

        void Update()
        {
            timeToLive += Time.deltaTime;
            if (timeToLive >= maxTimeToLive)
                Destroy(gameObject);
        }

        private void FixedUpdate()
        {
            MoveProjectile();
        }

        public void Initialize(Vector2 direction)
        {
            this.direction = direction;

            var rotation = Quaternion.LookRotation(direction, Vector3.forward).eulerAngles;
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, rotation.y);
        }

        private void MoveProjectile()
        {
            rb2D.MovePosition(rb2D.position + direction.normalized * speed * Time.fixedDeltaTime);
        }

    }
}

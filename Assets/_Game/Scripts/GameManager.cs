﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Com.LuisPedroFonseca.ProCamera2D;
using System;
using DG.Tweening;

namespace ShenniJam
{
    public class GameManager : UnitySingleton<GameManager>
    {
        private ProCamera2D proCamera2D;
        private ProCamera2DTransitionsFX proCamera2DTransitionsFX;
        private Player player;
        private Door door;

        [Header("Attributes")]
        public string[] sceneNames;
        public int currentScene = 0;

        protected override void Awake()
        {
            base.Awake();
        }

        public void InitializeLevel()
        {
            player = FindObjectOfType<Player>();
            door = FindObjectOfType<Door>();
            proCamera2D = FindObjectOfType<ProCamera2D>();
            proCamera2DTransitionsFX = proCamera2D.GetComponent<ProCamera2DTransitionsFX>();

            player.onTeleport += OnTeleport;
            player.onEndTeleport += OnEndTeleport;

            player.onSwitch += OnSwitch;
            player.onEndSwitch += OnEndSwitch;

            player.onDeath += OnDeath;

            proCamera2D.AddCameraTarget(player.transform);
        }

        private void OnDeath()
        {
            proCamera2DTransitionsFX.TransitionExit();
            DOVirtual.DelayedCall(2f, () =>
            {
                proCamera2DTransitionsFX.TransitionEnter();
            });
        }

        public void EndLevel()
        {
            player.onTeleport -= OnTeleport;
            player.onEndTeleport -= OnEndTeleport;

            player.onSwitch -= OnSwitch;
            player.onEndSwitch -= OnEndSwitch;

            player.onDeath -= OnDeath;
        }

        private void OnSwitch()
        {
            proCamera2D.Zoom(-3f, 1f);
            StartCoroutine(ZoomToDoorCR());
        }

        private IEnumerator ZoomToDoorCR()
        {
            yield return new WaitForSeconds(1f);

            proCamera2DTransitionsFX.TransitionExit();

            yield return new WaitForSeconds(1f);

            proCamera2D.Zoom(5.4f, 0);
            proCamera2D.RemoveAllCameraTargets();
            proCamera2D.AddCameraTarget(door.transform);

            proCamera2D.HorizontalFollowSmoothness = 0f;
            proCamera2D.VerticalFollowSmoothness = 0f;

            yield return new WaitForSeconds(1.5f);

            proCamera2DTransitionsFX.TransitionEnter();

            yield return new WaitForSeconds(1f);
            door.FallLock();

            yield return new WaitForSeconds(1.75f);

            proCamera2DTransitionsFX.TransitionExit();

            yield return new WaitForSeconds(1f);

            proCamera2D.RemoveAllCameraTargets();
            proCamera2D.AddCameraTarget(player.transform);

            yield return new WaitForSeconds(1f);

            proCamera2DTransitionsFX.TransitionEnter();
            proCamera2D.HorizontalFollowSmoothness = 0.05f;
            proCamera2D.VerticalFollowSmoothness = 0.05f;
            proCamera2D.Zoom(5.4f, 0f);
        }

        private void OnEndSwitch()
        {
            //proCamera2DTransitionsFX.TransitionEnter();
            //proCamera2D.Zoom(3f, 1f);
        }

        private void OnTeleport()
        {
            proCamera2D.Zoom(-3f, 1f);

            DOVirtual.DelayedCall(1f, () =>
            {
                proCamera2DTransitionsFX.TransitionExit();
            });
        }

        private void OnEndTeleport()
        {
            proCamera2D.Zoom(5.4f, 0f);
            DOVirtual.DelayedCall(1f, () =>
            {
                proCamera2DTransitionsFX.TransitionEnter();
            });
        }

        public void StartGame()
        {
            currentScene = 0;
            AudioManager.instance.PlayGameAudio();
        }

        public void LoadNextScene()
        {
            //EndLevel();

            AudioManager.instance.StopWalkSFX();

            if (sceneNames.Length <= currentScene + 1)
            {
                SceneManager.LoadScene("Credits");
                AudioManager.instance.StopGameAudio();
                AudioManager.instance.StopWalkSFX();
            }
            else
            {
                currentScene++;
                SceneManager.LoadScene(sceneNames[currentScene]);
            }
        }

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

namespace ShenniJam
{

    public class Door : MonoBehaviour
    {
        public List<GameObject> locks;
        public float moveX;

        private GameObject currentLock;
        private int lockQt;

        private void Awake()
        {
            var switches = GameObject.FindObjectsOfType<Switch>();
            lockQt = switches.Length;

            foreach (var l in locks)
                l.SetActive(false);

            for (int i = 0; i < lockQt; i++)
                locks[i].SetActive(true);
        }

        public void OpenLock()
        {
            lockQt--;
            currentLock = locks[lockQt];
        }

        public void FallLock()
        {
            currentLock.SetActive(false);

            if (lockQt == 0)
            {
                DOVirtual.DelayedCall(1f, () =>
                {
                    OpenDoor();
                });
                return;
            }
        }

        private void OpenDoor()
        {
            transform.DOMoveX(transform.position.x + moveX, 1f);
        }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlameThrower : MonoBehaviour
{
    [Header("Attributes")]
    public float rotationTime = 1f;
    public float rotationSpeed = 2f;
    public bool spinning = true;
    private Vector3 rotation;

    private IEnumerator Start()
    {
        rotation = new Vector3(0, 0, 0);
        while (spinning)
        {
            yield return new WaitForSeconds(rotationTime);
            rotation += new Vector3(0, 0, rotationSpeed);
            transform.rotation = Quaternion.Euler(rotation);
        }

        
    }

}

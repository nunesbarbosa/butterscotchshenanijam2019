﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace ShenniJam
{

    public class Portal : MonoBehaviour
    {
        [Header("Attributes")]
        public Portal portalTwin;
        public GameObject body;
        public float offset;
        public Collider2D myCollider;
        public ParticleSystem portalFX;

        private bool canTeleport = true;

        //Cached ref
        Player player;

        private void Awake()
        {
            player = FindObjectOfType<Player>();
            player.onEndTeleport += OnEndTeleport;
        }

        private void Update()
        {
            if (portalTwin)
            {
                TeleportPlayer();
            }
        }

        private void CreateBody()
        {
            player.body = (GameObject)Instantiate(body, player.transform.position, Quaternion.identity);

            player.body.GetComponent<Animator>().SetBool("Sleep", true);
            player.body.GetComponent<CharacterController>().CloseEyes();

            //animator.SetBool("Sleep", true);
            //characterController.CloseEyes();

        }

        private void TeleportPlayer()
        {
            if (myCollider.IsTouchingLayers(LayerMask.GetMask("Player")) && canTeleport)
            {
                canTeleport = false;
                AudioManager.instance.PlayTeleport();

                DOVirtual.DelayedCall(3f, () =>
                {
                    //CreateBody();
                    canTeleport = true;
                });

                player.TeleportPlayer(portalTwin, offset);

                portalFX.Play();
                portalTwin.portalFX.Play();
            }
        }

        private void OnEndTeleport()
        {
            portalFX.Stop();
        }

        //private void OnTriggerEnter2D(Collider2D collision)
        //{
        //    Player player = collision.gameObject.GetComponent<Player>();
        //    if (!player || !portalTwin) return;
        //
        //    if(Input.GetKeyDown(KeyCode.Space))
        //        player.TeleportPlayer(portalTwin);
        //}



    }
}

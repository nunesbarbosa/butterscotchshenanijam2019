﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Puppet2D;
using System;

namespace ShenniJam
{
    public class Enemy1Controller : MonoBehaviour
    {
        public float patrolSpeed;
        public float chaseSpeed;

        private EnemyPatrol enemyPatrol;
        private EnemySight enemySight;
        private Animator animator;
        private Puppet2D_GlobalControl puppet2DGlobalControl;
        private Player player;
        private Rigidbody2D rb2D;
        private bool isChasing;
        private bool canMove = true;

        private void Awake()
        {
            enemyPatrol = GetComponent<EnemyPatrol>();
            enemySight = GetComponent<EnemySight>();
            animator = GetComponentInChildren<Animator>();
            puppet2DGlobalControl = GetComponentInChildren<Puppet2D_GlobalControl>();
            player = GameObject.FindObjectOfType<Player>();
            rb2D = GetComponent<Rigidbody2D>();

            enemyPatrol.onMove += OnMove;
            enemySight.player = player.transform;
            enemySight.OnPlayerInSight += OnPlayerInSight;
            enemySight.OnPlayerOutOfSight += OnPlayerOutOfSight;
        }

        private void Start()
        {
            enemyPatrol.StartPatrol();
        }

        private void Update()
        {
            if (isChasing)
            {
                var direction = player.transform.position - transform.position;
                Move(direction);
            }
        }

        private void OnPlayerOutOfSight()
        {
            isChasing = false;
            enemyPatrol.StartPatrol();
        }

        private void OnPlayerInSight(Transform collision)
        {
            enemyPatrol.PausePatrol();
            isChasing = true;
        }

        private void OnMove(Vector2 direction)
        {
            Move(direction);
        }

        private void Move(Vector2 direction)
        {
            if (!canMove)
                return;

            animator.SetFloat("Speed", direction.magnitude);
            puppet2DGlobalControl.flip = direction.x > 0;

            rb2D.MovePosition(rb2D.position + direction.normalized * (isChasing ? chaseSpeed : patrolSpeed) * Time.fixedDeltaTime);
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag("Player"))
            {
                player.KillPlayer();
                animator.SetTrigger("Attack");
                canMove = false;

                StartCoroutine(RestartMoveCR());
            }
        }

        private IEnumerator RestartMoveCR()
        {
            yield return new WaitForSeconds(2f);
            canMove = true;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

namespace ShenniJam
{
    public class Pupil : MonoBehaviour
    {
        public GameObject pupil;

        public float pupilsSpeed;
        public float pupilsRadius = 0.5f;
        public float detectionDistance = 5f;

        private Transform lookAt;
        private Vector2 pupilsCenter;

        private bool isVisible = true;

        void Awake()
        {
            pupilsCenter = pupil.transform.localPosition;
            SetLookAt(GameObject.FindObjectOfType<Player>().transform);
        }

        void Update()
        {
            UpdatePupils();
        }

        private void UpdatePupils()
        {
            if (!isVisible)
                return;

            if (lookAt != null && Vector2.Distance(transform.position, lookAt.position) <= detectionDistance)
            {
                Vector3 lookAtVec = lookAt == null ? Vector3.zero : lookAt.position;

                Vector2 direction = lookAtVec - pupil.transform.position;
                direction = Vector2.ClampMagnitude(direction, pupilsRadius);

                pupil.transform.localPosition = Vector2.Lerp(pupil.transform.localPosition, pupilsCenter + direction, Time.deltaTime * pupilsSpeed);
            }
            else
            {
                pupil.transform.localPosition = Vector2.Lerp(pupil.transform.localPosition, pupilsCenter, Time.deltaTime * pupilsSpeed);
            }
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(transform.position, detectionDistance);
        }

        public void SetLookAt(Transform lookAt)
        {
            this.lookAt = lookAt;
        }

        private void OnBecameVisible()
        {
            isVisible = true;
        }

        private void OnBecameInvisible()
        {
            isVisible = false;
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace ShenniJam
{
    public class Switch : MonoBehaviour
    {
        public GameObject switchOn;
        public GameObject switchOff;

        private Collider2D col2D;

        [Header("Attributes")]
        public Door door;

        private void Awake()
        {
            switchOn.SetActive(false);
            col2D = GetComponent<Collider2D>();
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            Player player = collision.gameObject.GetComponent<Player>();
            if (!player) return;

            if (player.PressSwitch() && door)
            {
                AudioManager.instance.PlaySwitch();

                switchOn.SetActive(true);
                switchOff.SetActive(false);

                door.OpenLock();
                col2D.enabled = false;
            }
        }



    }

}
